from django.db import models


class UploadedFile(models.Model):
    NOT_JOIN_WITH_TABLE = True

    author = models.ForeignKey(User, related_name='uploaded_files', verbose_name='Автор',
                               null=True, on_delete=models.SET_NULL)

    timestamp = models.DateTimeField('Дата добавления', auto_now_add=True)
    name = models.CharField('Имя файла', max_length=500)
    description = DescriptionCharField(max_length=1000)
    file = models.FileField('Файл', upload_to=dynamic_path, max_length=255)
    old_file = models.FileField('Файл: Удалить поле', upload_to=dynamic_path, null=True, max_length=255)
    size = models.CharField('Размер в тексте', null=True, blank=True, max_length=100)
    file_type = models.CharField('Тип файла', null=True, blank=True, max_length=20)
    params = models.JSONField('Дополнительный параметры', null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.file:
            self.size = sizeof_fmt(self.file.size)

        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Присоединенный файл"
        verbose_name_plural = "Присоединенные файлы"

    def __str__(self):
        s = f"Файл id: {self.id} размер: {self.size}"
        return s
