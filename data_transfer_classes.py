"""
    Классы для экспорта и импорта структуры данных
"""
import datetime
import io
import json
import logging
import os
import queue
import shutil
import time
import traceback
import zipfile
from json import JSONDecodeError
from pathlib import Path
from typing import Any, Union

from django.apps import apps
from django.core.files import File
from django.db.models import FileField, CharField, DateTimeField, JSONField
from django.db.models.fields.files import FieldFile

from file_upload.models import UploadedFile
from workflow.models import DocType, DocTypeImportLog, DataStructureSettings, get_default_json_schemas

logger = logging.getLogger('b3.data_structure_transfer')


class BaseStructureClass:
    DEFAULT_UNIQUE_FIELD_NAMES = ('code', 'name', 'slug')
    SEPARATOR_CHAR = '&'
    JSON_SCHEMAS: dict

    models_dict: dict

    def __init__(self, user_id: int):
        self.log = ''
        self.start_dt = datetime.datetime.now()
        self.user_id = user_id
        self.log_record_id = None
        self.start_objects = []

        settings = DataStructureSettings.objects.filter(active=True).last()
        if settings:
            self.DEFAULT_UNIQUE_FIELD_NAMES = settings.default_unique_key_fields
            self.SEPARATOR_CHAR = settings.separator_char

    def save_log_record(self, operation: int = DocTypeImportLog.OPERATION_IMPORT,
                        status: int = None, zip_buffer: io.BytesIO = None, file_id: int = None) -> None:
        """ Запись лога в DocTypeImportLog """
        if not self.log_record_id:
            log_record = DocTypeImportLog(
                operation=operation,
                start=self.start_dt,
                status=DocTypeImportLog.IN_PROGRESS,
                author_id=self.user_id,
                file_id=file_id,
                high_level_objects=self.start_objects,
            )
            log_record.save()
            self.log_record_id = log_record.id
        else:
            _now = datetime.datetime.now()
            delta = datetime.datetime.now() - self.start_dt
            log_record = DocTypeImportLog.objects.filter(id=self.log_record_id).first()
            log_record.end = _now
            log_record.duration = int(delta.total_seconds())
            log_record.status = status
            log_record.execution_log = self.log
            zip_name = f'doctype_structure-{_now:%Y-%m-%d_%H-%M-%S}.zip'
            log_record.zip_file = File(zip_buffer, name=zip_name) if zip_buffer else None
            log_record.save()

    def log_message(self, message: str) -> None:
        logger.info(message)
        self.log += message + '\n'

    @staticmethod
    def get_obj_model_name(obj: Any) -> str:
        return f'{obj._meta.app_label}.{obj._meta.model_name}' if getattr(obj, '_meta', None) else None

    def get_model_by_name(self, full_model_name: str) -> Any:
        if full_model_name in self.models_dict:
            return self.models_dict[full_model_name]['model']
        else:
            app_label, model_name = full_model_name.split(".")
            model = apps.get_model(app_label, model_name)
            self.models_dict[full_model_name] = {'model': model}
            return model

    def tuple_to_str(self, t: Union[tuple, list]) -> str:
        """ Кортеж или список - в строку формата (t1,t2,...) """
        t = [item if item else ' ' for item in t]
        s = t[0]
        for item in t[1:]:
            s += self.SEPARATOR_CHAR + item
        return f'({s})'

    @staticmethod
    def get_closing_bracket_pos(s: str) -> int:
        """ Позиция ближайшей закрывающей скобки в строке """
        n = 1
        pos = 0
        while pos < len(s) and n > 0:
            pos += 1
            if s[pos] == ')':
                n -= 1
            elif s[pos] == '(':
                n += 1
        return pos

    def str_to_tuple(self, s: str) -> Any:
        """ Строка типа (t1,t2,(x1,x2),t3) - в кортеж """
        if not s or not s.startswith('('):
            return s
        s = s[1:-1]
        res = []
        while len(s):
            if s.startswith('('):
                pos = self.get_closing_bracket_pos(s)
                res.append(self.str_to_tuple(s[:pos + 1]))
                s = s[pos + 2:] if pos + 2 < len(s) else ''
            else:
                tmp = s.split(self.SEPARATOR_CHAR, 1)
                res.append(tmp[0])
                s = tmp[1] if len(tmp) > 1 else ''
        res = [None if ss == ' ' else ss for ss in res]
        return tuple(res)

    def get_unique_key_from_model_without_unique_fields(self, model: Any) -> Any:
        """ Уникальный код модели без уникальных полей """
        model_meta_class = model._meta
        if getattr(model_meta_class, 'unique_together'):
            key = model_meta_class.unique_together[0]  # return first tuple from tuple of tuples
            return self.tuple_to_str(key)
        else:   # делаем ключ из неуникального поля
            fields = [f.name for f in model_meta_class.fields if not f.auto_created and
                      not f.primary_key and type(f) in (CharField,)]
            for field_name in self.DEFAULT_UNIQUE_FIELD_NAMES:
                if field_name in fields:
                    return field_name
            return fields[0] if fields else None

    def get_model_unique_key(self, model: Any) -> Any:
        """ Уникальный код для однозначной идентификации объектов модели """
        model_meta_class = model._meta
        unique_fields = [f.name for f in model_meta_class.fields
                         if f.unique and not f.auto_created and not f.primary_key and not f.is_relation]
        if not unique_fields:
            return self.get_unique_key_from_model_without_unique_fields(model)
        for field_name in self.DEFAULT_UNIQUE_FIELD_NAMES:
            if field_name in unique_fields:
                return field_name
        return unique_fields[0]  # если нет в дефолтном списке, возвращаем первое найденное

    def get_unique_key(self, model_name: str) -> Any:
        """ Уникальный код для однозначной идентификации объектов модели """
        if model_name in self.models_dict and 'unique_field_name' in self.models_dict[model_name]:
            return self.models_dict[model_name]['unique_field_name']
        else:
            raise ValueError(f'Модель {model_name} не инициирована или не имеет unique_field_name')

    @staticmethod
    def add_complex_key(field: str, pos_start: int, pos_end: int, fields_dict: dict) -> None:
        start_str = field[:pos_start]
        end_str = field[pos_end + 1:]
        complex_key_model_name = field[pos_start + 1:pos_end]
        if '_complex_keys' not in fields_dict:
            fields_dict['_complex_keys'] = {}
        fields_dict['_complex_keys'][(start_str, end_str)] = {
            "complex_key_model_name": complex_key_model_name,
            "value_model_name": fields_dict[field],
        }

    def init_json_schemas(self) -> None:
        """
        Находит в JSON-полях JSON_SCHEMAS комплексные ключи вида td_<workflow.doctype>_form_list, парсит их и
        сохраняет в структуре _complex_keys
        """
        for model, json_fields_dict in self.JSON_SCHEMAS.items():
            for json_field, fields_dict in json_fields_dict.items():
                for field in list(fields_dict.keys()):
                    pos_start = field.find('<')
                    if pos_start == -1:
                        continue
                    pos_end = field.find('>')
                    if pos_end == -1:
                        continue
                    self.add_complex_key(field, pos_start, pos_end, fields_dict)

    @staticmethod
    def check_complex_key(key: str, keys_dict: dict) -> (str, tuple, dict):
        """
        Проверяет ключ словаря на вхождение в список комплексных ключей.
        Возвращает значение внутри комплексного ключа и модель для преобразования значения ключа,
        """
        for (start_str, end_str), key_models_dict in keys_dict['_complex_keys'].items():
            if key.startswith(start_str) and key.endswith(end_str):
                complex_key_content = key[len(start_str):-len(end_str)]
                return complex_key_content, (start_str, end_str), key_models_dict
            return None, None, None


class ExportStructure(BaseStructureClass):
    APP_LABELS = ()
    EXCLUDED_MODELS = ()
    EXCLUDE_HIGH_LEVEL_MANY_TO_MANY = False
    ONLY_START_OBJECTS = False
    TREE_TRAVERSAL_STRAIGHT_DOWN = True
    JSON_SCHEMAS = {}
    IGNORE_JSON_VALUE_ERRORS = False

    def __init__(self, start_objects: list, user_id: int):
        super(ExportStructure, self).__init__(user_id)

        self.start_objects = start_objects
        self.start_objects_unique_key_values = []
        self.save_log_record(operation=DocTypeImportLog.OPERATION_EXPORT)
        self.files = []
        self.existed_objects = {}
        self.models_dict = {}
        self.ids_unique_keys_dict = {}
        self.high_level_model_name = self.get_obj_model_name(start_objects[0])

        self.queue = queue.Queue()
        if not self.APP_LABELS:
            self.APP_LABELS = (start_objects[0]._meta.app_label)

        self.init_json_schemas()
        self.init_models_dict()

        for obj in start_objects:
            obj_unique_key_value = self.get_obj_unique_key_value(obj, self.high_level_model_name)
            self.start_objects_unique_key_values.append(obj_unique_key_value)
            self.queue.put(obj)

    def check_model(self, model: Any) -> bool:
        """ Проверяет модель на выполнение начальных условий """
        model_name = self.get_obj_model_name(model)
        if model._meta.app_label in self.APP_LABELS and model_name not in self.EXCLUDED_MODELS:
            return True
        else:
            return False

    def add_low_level_model(self, field: Any, low_level_models_list: list) -> None:
        """ Добавляет связанную модель в список моделей нижнего уровня """
        model_name = self.get_obj_model_name(field.related_model)
        if self.check_model(field.related_model) and model_name not in self.models_dict:
            low_level_model_name = self.get_obj_model_name(field.related_model)
            if low_level_model_name not in low_level_models_list:
                low_level_models_list.append(low_level_model_name)

    def get_model_obj_fields(self, model: Any, low_level_models_list: list) -> dict:
        """ Возвращает словарь полей модели и дополняет список моделей нижнего уровня """
        obj_fields = {}
        model_meta_class = model._meta
        for f in model_meta_class.fields:
            if f.is_relation:
                self.add_low_level_model(f, low_level_models_list)
                if self.check_model(f.related_model):
                    obj_fields[f.name] = f
            # исключаем поля типа дата-время, так как в конструкторе такие поля не имеют полезной нагрузки
            elif not isinstance(f, DateTimeField) and not f.auto_created:
                obj_fields[f.name] = f

        return obj_fields

    def get_model_multi_fields(self, model: Any, low_level_models_list: list) -> dict:
        """ Возвращает словарь связанных и m2m полей модели и дополняет список моделей нижнего уровня """
        multi_fields = {}
        model_meta_class = model._meta
        m2m_objects = getattr(model_meta_class, 'many_to_many', [])
        for f in m2m_objects:
            self.add_low_level_model(f, low_level_models_list)
            if self.check_model(f.related_model) and not self.is_high_level_model(f.related_model):
                multi_fields[f.name] = f

        related_objects = getattr(model_meta_class, 'related_objects', [])
        for f in related_objects:
            self.add_low_level_model(f, low_level_models_list)
            # оставляем связи только с моделями из предопределенных приложений и исключаем обратные many-to-many
            if self.check_model(f.related_model) and not f.many_to_many and \
                    not self.is_high_level_model(f.related_model):
                field_name = f.related_name if getattr(f, 'related_name', None) else f.name + '_set'
                multi_fields[field_name] = f

        return multi_fields

    def init_model(self, model_name: str, level: int = 0, low_level_models_list: list = None) -> None:
        """ Сохраняет модель и ее уровень в словарь models_dict и определяет дочерние модели низшего уровня """
        if model_name in self.models_dict:
            return
        model = self.get_model_by_name(model_name)
        model_meta_class = model._meta
        if model_meta_class.app_label in self.APP_LABELS and model_name not in self.EXCLUDED_MODELS:
            self.models_dict[model_name] = {}
            self.models_dict[model_name]['model'] = model
            self.models_dict[model_name]['level'] = level
            self.models_dict[model_name]['unique_field_name'] = self.get_model_unique_key(model)
            self.models_dict[model_name]['obj_fields'] = self.get_model_obj_fields(model, low_level_models_list)
            self.models_dict[model_name]['multi_fields'] = self.get_model_multi_fields(model, low_level_models_list)

    def init_models_dict(self, level: int = 0, models_list: list = None) -> None:
        """ Заполняет словарь models_dict и проставляет моделям уровни относительно модели верхнего уровня """

        # если на очередном шаге не обнаружено новых моделей низшего уровня, то выходим
        if level and not models_list:
            return

        # нулевой уровень
        if level == 0:
            models_list = [self.high_level_model_name]

        low_level_models = []
        for model_name in models_list:
            self.init_model(model_name, level, low_level_models)
        level += 1
        self.init_models_dict(level, low_level_models)

    def get_model_level(self, model_name: str) -> int:
        return self.models_dict[model_name]['level']

    def get_obj_model(self, obj: Any) -> Any:
        model_name = self.get_obj_model_name(obj)
        return self.get_model_by_name(model_name)

    def get_obj_unique_key(self, obj: Any) -> Any:
        model_name = self.get_obj_model_name(obj)
        return self.get_unique_key(model_name)

    def is_high_level_model(self, model):
        if self.EXCLUDE_HIGH_LEVEL_MANY_TO_MANY:
            model_name = self.get_obj_model_name(model)
            if model_name == self.high_level_model_name:
                return True
        return False

    def is_start_object(self, unique_key_value: str, model_name: str) -> bool:
        if model_name == self.high_level_model_name and \
                self.ONLY_START_OBJECTS and unique_key_value in self.start_objects_unique_key_values:
            return True
        return False

    def get_obj_fields(self, model_name):
        """
        Возвращает словарь полей объекта. obj_fields - простые поля, включая внешние ключи.
        multi_fields - many_to_many и обратные внешние ключи
        """
        if 'obj_fields' in self.models_dict[model_name] and 'multi_fields' in self.models_dict[model_name]:
            return self.models_dict[model_name]['obj_fields'], self.models_dict[model_name]['multi_fields']
        else:
            raise ValueError(f'Модель {model_name} не инициирована или не имеет obj_fields и multi_fields')

    def get_obj_unique_key_value(self, obj, model_name):
        """ Значение уникального ключа """
        key = self.get_unique_key(model_name)
        if not key:
            return None
        if key.startswith('('):
            key = self.str_to_tuple(key)
            key_value = []
            for field_name in key:
                value = getattr(obj, field_name)
                if hasattr(value, '_meta'):
                    related_model_name = self.get_obj_model_name(value)
                    value = self.get_obj_unique_key_value(value, related_model_name)
                key_value.append(value)
            return self.tuple_to_str(key_value)
        return getattr(obj, key)

    def get_obj_unique_key_value_by_id(self, obj_id: int, model_name: str) -> str:
        """ Значение уникального ключа по id"""
        obj_unique_key_value = None
        if model_name in self.ids_unique_keys_dict and obj_id in self.ids_unique_keys_dict[model_name]:
            obj_unique_key_value = self.ids_unique_keys_dict[model_name][obj_id]
        else:
            model = self.get_model_by_name(model_name)
            obj = model.objects.filter(id=obj_id).first()
            if obj:
                obj_unique_key_value = self.get_obj_unique_key_value(obj, model_name)
                if not (self.ONLY_START_OBJECTS and model_name == self.high_level_model_name) and \
                        not self.TREE_TRAVERSAL_STRAIGHT_DOWN:
                    self.queue.put(obj)  # помещаем объект в очередь на обработку
                if model_name not in self.ids_unique_keys_dict:
                    self.ids_unique_keys_dict[model_name] = {}
                self.ids_unique_keys_dict[model_name][obj_id] = obj_unique_key_value
        return obj_unique_key_value

    def get_converted_complex_key(self, obj_id: str, start_end: tuple, models_dict: dict) -> (str, str):
        """
        Проверяет ключ словаря на вхождение в список комплексных ключей (с id в имени ключа)0.
        Возвращает преобразованный ключ и модель для преобразования значения ключа (если есть)
        """
        obj_unique_key_value = self.get_obj_unique_key_value_by_id(
            int(obj_id),
            models_dict['complex_key_model_name']
        )
        if not obj_unique_key_value and not self.IGNORE_JSON_VALUE_ERRORS:
            raise ValueError(f'JSON complex key error. {start_end[0]}{obj_id}{start_end[1]} is undefined')

        return f'{start_end[0]}{obj_unique_key_value}{start_end[1]}', models_dict['value_model_name']

    def replace_simple_id(self, key: str, dct: dict, keys_dict: dict, obj_unique_key_value: str) -> None:
        model_name = keys_dict[key]
        val = dct[key]
        if isinstance(val, int):
            dct[key] = self.get_obj_unique_key_value_by_id(dct[key], model_name)
            if not dct[key] and not self.IGNORE_JSON_VALUE_ERRORS:
                raise ValueError(
                    f'JSON field error. Model {model_name}, object {obj_unique_key_value}, '
                    f'field {key}, id {val} not found')
        elif isinstance(val, list):
            tmp_list = []
            for obj_id in val:
                if not isinstance(obj_id, int):
                    raise ValueError(f'Объект {obj_unique_key_value}. JSON поле типа список {key} '
                                     f'содержит значение {obj_id} - не int')
                obj_unique_key_value = self.get_obj_unique_key_value_by_id(obj_id, model_name)
                if not obj_unique_key_value and not self.IGNORE_JSON_VALUE_ERRORS:
                    raise ValueError(
                        f'JSON field error. Model {model_name}, object {obj_unique_key_value}, '
                        f'field {key}, id {obj_id} not found')
                tmp_list.append(obj_unique_key_value)
            dct[key] = tmp_list

    def replace_complex_id(self, key: str, dct: dict, keys_dict: dict, obj_unique_key_value: str) -> None:
        # проверяем является ли комплексным ключом
        obj_id, start_end_tuple, models_dict = self.check_complex_key(key, keys_dict)
        if obj_id:
            converted_key, value_model_name = self.get_converted_complex_key(
                obj_id, start_end_tuple, models_dict)

            # заменяем ключ в словаре
            val = dct.pop(key)
            dct[converted_key] = val

            dct[converted_key] = self.get_obj_unique_key_value_by_id(dct[converted_key], value_model_name)
            if not dct[converted_key] and not self.IGNORE_JSON_VALUE_ERRORS:
                raise ValueError(
                    f'JSON field error. Model {value_model_name}, object {obj_unique_key_value}, '
                    f'field {key}, id {val} not found')

    def replace_dict_ids(self, dct: dict, keys_dict: dict, obj_unique_key_value: str) -> dict:
        """ В словаре заменяет значения id на уникальные ключи """
        for key in list(dct.keys()):
            if isinstance(dct[key], dict):
                dct[key] = self.replace_dict_ids(dct[key], keys_dict, obj_unique_key_value)
            if key in keys_dict and (isinstance(dct[key], int) or isinstance(dct[key], list)):
                self.replace_simple_id(key, dct, keys_dict, obj_unique_key_value)
            elif key not in keys_dict:
                self.replace_complex_id(key, dct, keys_dict, obj_unique_key_value)
        return dct

    def replace_json_field_ids(
            self, json_obj: dict, model_name: str, field_name: str, obj_unique_key_value: str) -> dict:
        """
        Преобразовывает значение поля типа JSON, если это поле задано в JSON_SCHEMAS.
        Меняет значения id на уникальные ключи.
        """
        if model_name in self.JSON_SCHEMAS and field_name in self.JSON_SCHEMAS[model_name]:
            return self.replace_dict_ids(json_obj, self.JSON_SCHEMAS[model_name][field_name], obj_unique_key_value)
        return json_obj

    def get_simple_field_value(self, value: Any, model_name: str, field_name: str, obj_unique_key_value: str) -> Any:
        """ Получение значения атрибута объекта по названию поля """
        if type(value) in (FileField, FieldFile):
            self.files.append(value.name)
            return value.name
        elif isinstance(value, dict):  # JSON field
            value = self.replace_json_field_ids(value, model_name, field_name, obj_unique_key_value)
            return value
        else:
            return value

    def check_models_levels(self, parent_model: str, child_model: str):
        if not self.TREE_TRAVERSAL_STRAIGHT_DOWN or self.TREE_TRAVERSAL_STRAIGHT_DOWN and self.get_model_level(
                parent_model) < self.get_model_level(child_model):
            return True
        else:
            return False

    def check_related_model(self, parent_model: str, child_model: str) -> bool:
        if self.check_models_levels(parent_model, child_model) and not self.is_high_level_model(child_model):
            return True
        return False

    def get_fk_field_value(self, value: Any, model_name: str) -> str:
        """ Получение значения уникального ключа связанного объекта"""
        related_model_name = self.get_obj_model_name(value)
        unique_key_value = self.get_obj_unique_key_value(value, related_model_name)
        if unique_key_value and (related_model_name not in self.existed_objects or
                                 unique_key_value not in self.existed_objects[related_model_name]) \
                and self.check_related_model(model_name, related_model_name) and \
                not self.is_start_object(unique_key_value, related_model_name):
            self.queue.put(value)
        return unique_key_value

    def get_field_value(self, obj: Any, model_name: str, field_name: str, field_obj: Any,
                        obj_unique_key_value: str) -> Any:
        """ Получение значения атрибута объекта по названию поля """
        try:
            value = getattr(obj, field_name)
        except Exception as err:
            logger.info(traceback.format_exc())
            raise ValueError(f'Модель: {model_name}. Объект: {obj}. Получение значения поля {field_name}. Ошибка {err}')
        # если поле типа FieldFile, FileField не заполнено, то возвращает пустое значение типа FieldFile, FileField,
        # что приводит к ошибке при json.dumps
        if value == '' and type(value) in (FieldFile, FileField):
            return None
        if value is None or value == '':
            return value
        if not field_obj.is_relation:  # простое поле
            return self.get_simple_field_value(value, model_name, field_name, obj_unique_key_value)
        else:  # foreign key
            return self.get_fk_field_value(value, model_name)

    def get_multi_field_value(self, obj: Any, field_name: str, field_obj: Any, model_name: str) -> Union[list, None]:
        """ Получение значений атрибута типа m2m по названию поля """
        related_model_name = self.get_obj_model_name(field_obj.related_model)
        if related_model_name not in self.models_dict or field_obj.one_to_one:
            return None
        m2m_set = getattr(obj, field_name)
        related_objects = list()
        try:
            num = m2m_set.count()
        except Exception:
            return None
        self.log_message(f'Связанный объект {field_name}, кол-во элементов {num}')
        check_related_model = self.check_related_model(model_name, related_model_name)
        for q in m2m_set.all():
            unique_key_value = self.get_obj_unique_key_value(q, related_model_name)
            related_objects.append(unique_key_value)
            if check_related_model and not self.is_start_object(unique_key_value, related_model_name):
                self.queue.put(q)

        if field_obj.many_to_many:  # сохраняем в списке полей объекта только many-to-many
            return related_objects
        else:
            return None

    def get_object_fields_values(self, obj: Any, model_name: str, obj_unique_key_value: str) -> dict:
        """ Получение словаря атрибутов объекта со значениями """
        self.log_message(f'...Обработка. Модель {model_name}  Объект {obj_unique_key_value}')
        fields_values_dict = {}
        obj_fields, multi_fields = self.get_obj_fields(model_name)
        for field_name, field_obj in obj_fields.items():
            fields_values_dict[field_name] = self.get_field_value(
                obj, model_name, field_name, field_obj, obj_unique_key_value)
        for field_name, field_obj in multi_fields.items():
            related_objects = self.get_multi_field_value(obj, field_name, field_obj, model_name)
            if related_objects:
                fields_values_dict[field_name] = related_objects
        return fields_values_dict

    def process_object(self, obj):
        """ Обработка объекта """
        model_name = self.get_obj_model_name(obj)
        unique_value = self.get_obj_unique_key_value(obj, model_name)
        if model_name not in self.existed_objects:
            self.existed_objects[model_name] = {}
        if unique_value and unique_value not in self.existed_objects[model_name]:
            self.existed_objects[model_name][unique_value] = self.get_object_fields_values(
                obj, model_name, unique_value)

    def process_objects(self) -> None:
        """ Обработка объектов из очереди """
        start_time = time.monotonic()
        obj = None
        try:
            while not self.queue.empty():
                obj = self.queue.get()
                self.process_object(obj)
        except Exception as err:
            logger.info(traceback.format_exc())
            self.log_message(f'Ошибка при обработке объекта {obj}. {err}')
            self.save_log_record(operation=DocTypeImportLog.OPERATION_EXPORT, status=DocTypeImportLog.ERROR)
        else:
            self.log_message(f'Время выполнения {int(time.monotonic() - start_time)} секунд')
            zip_buffer = self.create_zip_archive()
            self.save_log_record(operation=DocTypeImportLog.OPERATION_EXPORT, status=DocTypeImportLog.SUCCESS,
                                 zip_buffer=zip_buffer)

    def clear_json_schemas(self) -> None:
        """ Удаление из JSON-схемы вспомогательного поля _complex_keys """
        for model, json_fields_dict in self.JSON_SCHEMAS.items():
            for json_field, fields_dict in json_fields_dict.items():
                if '_complex_keys' in fields_dict:
                    fields_dict.pop('_complex_keys')

    def get_structure(self) -> dict:
        """ Получение структуры данных в виде словаря """
        for model_name in self.existed_objects:
            self.existed_objects[model_name]['_unique_key'] = self.models_dict[model_name]['unique_field_name']
        self.clear_json_schemas()
        self.existed_objects['_json_schemas'] = self.JSON_SCHEMAS
        return self.existed_objects

    def get_json_structure(self) -> (str, list):
        """ Получение структуры данных в виде строки"""
        structure = self.get_structure()
        try:
            json_dump = json.dumps(structure, ensure_ascii=False, indent=3)
        except TypeError as err:
            raise TypeError(err)
        return json_dump, self.files

    def create_zip_archive(self) -> io.BytesIO:
        """ Создание zip-архива с выгруженной структурой """
        from workflow.admin import DocTypeAdmin

        content, files = self.get_json_structure()
        zip_buffer = io.BytesIO()
        with zipfile.ZipFile(zip_buffer, "a", zipfile.ZIP_DEFLATED, False) as zip_file:
            file_name = f'{self.start_objects[0]}.json'
            json_file = zipfile.ZipInfo(file_name)
            zip_file.writestr(json_file, content)
            DocTypeAdmin.add_files_to_zip(zip_file, files)
        return zip_buffer


class ExportDocTypeStructure(ExportStructure):
    APP_LABELS = ('workflow', 'form_builder', 'rules', 'contenttypes', 'eav', 'showcase_builder')
    EXCLUDED_MODELS = (
        'workflow.logevent',
        'workflow.docitem',
        'workflow.doctemplatescansignatory',
        'workflow.doctemplatescan',
        'workflow.doctemplatedoctypemembership',
        'workflow.doctypeimportlog',
        'eav.value',
        'showcase_builder.matviewtablelog',
    )
    EXCLUDE_HIGH_LEVEL_MANY_TO_MANY = True
    ONLY_START_OBJECTS = False
    JSON_SCHEMAS = get_default_json_schemas()

    def __init__(self, doc_type_codes: list, user_id: int):
        qs = list(DocType.objects.filter(code__in=doc_type_codes))
        settings = DataStructureSettings.objects.filter(active=True).last()
        if settings:
            self.APP_LABELS = settings.default_app_labels
            self.EXCLUDED_MODELS = [model_name.lower() for model_name in settings.excluded_models]
            self.EXCLUDE_HIGH_LEVEL_MANY_TO_MANY = settings.exclude_high_level_m2m
            self.ONLY_START_OBJECTS = settings.only_start_objects
            self.TREE_TRAVERSAL_STRAIGHT_DOWN = settings.tree_traversal_straight_down
            self.JSON_SCHEMAS = settings.json_schemas
        super(ExportDocTypeStructure, self).__init__(qs, user_id)


class ImportStructure(BaseStructureClass):
    IGNORE_JSON_VALUE_ERRORS = False
    CLEAR_OLD_M2M_RELATIONS = True

    def __init__(self, user_id: int, uploaded_file_id: int):
        self.tmp_folder = '/tmp/doctype_structures/' # //NOSONAR python:S5443 temp dir
        self.dir_path = Path(self.tmp_folder)
        self.extract_zip_archive(uploaded_file_id)

        _objects_dict = self.get_structure_from_dir()
        self.JSON_SCHEMAS = _objects_dict.pop('_json_schemas')
        self.objects_dict = _objects_dict
        self.created_objects = {}
        self.processed_objects = {}
        self.opened_files = []

        settings = DataStructureSettings.objects.filter(active=True).last()
        if settings:
            self.IGNORE_JSON_VALUE_ERRORS = settings.ignore_json_errors
            self.CLEAR_OLD_M2M_RELATIONS = settings.clear_old_m2m_relations

        self.models_dict = {}
        self.init_models_dict()
        self.init_json_schemas()

        super(ImportStructure, self).__init__(user_id)
        self.save_log_record(file_id=uploaded_file_id)

    def extract_zip_archive(self, uploaded_file_id: int):
        """ Распаковка архива во временную папку """
        uploaded_file = UploadedFile.objects.filter(id=uploaded_file_id).first()
        zip_file = uploaded_file.file

        # Удаляем старую временную структуру, если есть
        if self.dir_path.exists() and self.dir_path.is_dir():
            shutil.rmtree(self.dir_path)

        with zipfile.ZipFile(zip_file, 'r') as zipObj:
            zipObj.extractall(self.dir_path)

    def get_structure_from_dir(self) -> dict:
        """ Получение словаря объектов из JSON-файлов заданной директории """
        objects_dict = {}
        for (address, dirs, filenames) in os.walk(self.tmp_folder):
            for f in filenames:
                full_address = os.path.join(address, f)
                if f.endswith('.json'):
                    with open(full_address) as json_file:
                        try:
                            objects_dict = json.load(json_file)
                        except JSONDecodeError:
                            self.log_message(f'Неверная JSON-структура в файле {f}')
                    break
        return objects_dict

    def init_model_fields(self, model, model_name):
        """ Заполнение словаря полей модели """
        obj_fields = {}
        fk_fields = {}
        model_meta_class = model._meta
        for f in model_meta_class.fields:
            if f.is_relation:
                fk_fields[f.name] = f
            elif isinstance(f, DateTimeField) and (f.auto_now or f.auto_now_add):
                ...
            elif not f.auto_created and not f.primary_key:
                obj_fields[f.name] = f

        m2m_fields = {}
        if getattr(model_meta_class, 'many_to_many', None):
            for f in model_meta_class.many_to_many:
                m2m_fields[f.name] = f

        self.models_dict[model_name]['obj_fields'] = obj_fields
        self.models_dict[model_name]['fk_fields'] = fk_fields
        self.models_dict[model_name]['m2m_fields'] = m2m_fields

    def init_model(self, model_name: str):
        """ Заполнение уникального ключа модели """
        model = self.get_model_by_name(model_name)
        unique_key = self.objects_dict[model_name].pop('_unique_key')
        self.models_dict[model_name]['unique_key'] = self.str_to_tuple(unique_key)
        self.init_model_fields(model, model_name)

    def init_models_dict(self):
        """ Инициирование словаря моделей всеми необходимыми данными """
        for model_name in list(self.objects_dict):
            self.init_model(model_name)
            self.created_objects[model_name] = {}
            self.processed_objects[model_name] = {}

    def get_fk_key_and_value(self, key: str, key_value: Any, parent_obj_unique_key: Any, model_name: str) -> (str, Any):
        """ Получение названия и значения внешнего ключа (уникальный код связанного объекта) """
        if not key_value:  # пустой внешний ключ
            return key + '_id', key_value

        fk_field = self.models_dict[model_name]['fk_fields'][key]
        related_model = getattr(fk_field, 'related_model')
        related_model_name = self.get_obj_model_name(related_model)
        if type(key_value) in (list, tuple):
            key_value = self.tuple_to_str(key_value)
        if key_value in self.objects_dict[related_model_name]:
            # исключение рекурсии - объект имеет внешний ключ сам на себя
            if related_model_name == model_name and key_value == parent_obj_unique_key:
                related_obj_id = None
            else:
                related_obj_id, _ = self.get_or_create_object(key_value, related_model_name)
        else:
            related_obj_id, _ = self.get_or_create_object(key_value, related_model_name)
        return key + '_id', related_obj_id

    def get_related_model_name(self, field_obj: Any) -> str:
        model = field_obj.related_model
        return self.get_obj_model_name(model)

    def get_key_dict(self, key: Any, key_value: Any, model_name: str) -> dict:
        """ Получение словаря полей и значений объекта """
        res = {}
        for k, v in zip(key, key_value):
            if k in self.models_dict[model_name]['obj_fields']:
                res[k] = v
            elif k in self.models_dict[model_name]['fk_fields']:
                k, related_obj_id = self.get_fk_key_and_value(k, v, key_value, model_name)
                res[k] = related_obj_id
        return res

    def get_object_from_db(self, key_value: str, model_name: str) -> Any:
        """ Получение объекта из БД по уникальному ключу """
        model = self.get_model_by_name(model_name)
        if key_value and key_value.startswith('('):
            key_value = self.str_to_tuple(key_value)
            key_dict = self.get_key_dict(self.models_dict[model_name]['unique_key'], key_value, model_name)
        else:
            key_dict = {
                self.models_dict[model_name]['unique_key']: key_value
            }
        db_obj = model.objects.filter(
            **key_dict
        ).first()
        return db_obj

    def get_converted_complex_key(self, unique_key_value: str, start_end: tuple, models_dict: dict, obj_unique_key: str
                                  ) -> (str, str):
        """
        Конвертирует комплексный ключ. Возвращает преобразованный ключ и модель для преобразования значения ключа
        """
        obj_id, in_processing = self.get_or_create_object(unique_key_value, models_dict['complex_key_model_name'])

        if not obj_id and not self.IGNORE_JSON_VALUE_ERRORS:
            err_msg = f'JSON complex key error in {obj_unique_key}. ' \
                      f'{start_end[0]}{unique_key_value}{start_end[1]} is undefined. ' \
                      f'Unique key {unique_key_value} of model {models_dict["complex_key_model_name"]}'
            if not in_processing:
                err_msg += f' does not exist either in structure nor in DB'
            else:
                err_msg += f' - attempt to create when processing (cycling error)'
            self.log += err_msg
            self.save_log_record(status=DocTypeImportLog.ERROR)
            raise ValueError(err_msg)

        return f'{start_end[0]}{obj_id}{start_end[1]}', models_dict['value_model_name']

    def convert_json_field_value(self, dct: dict, json_schema: dict, obj_unique_key: str) -> dict:  # NOSONAR
        """ Преобразовывает словарь из JSON-поля, заменяя уникальные ключи объектов на ID согласно JSON-схеме """
        for key in list(dct.keys()):
            if isinstance(dct[key], dict):  # рекурсия
                dct[key] = self.convert_json_field_value(dct[key], json_schema, obj_unique_key)
            elif key in json_schema:
                model_name = json_schema[key]
                if isinstance(dct[key], list):
                    ids_list = []
                    for value in dct[key]:
                        obj_id, _ = self.get_or_create_object(value, model_name)
                        ids_list.append(obj_id)
                    dct[key] = ids_list
                else:
                    obj_id, _ = self.get_or_create_object(dct[key], model_name)
                    dct[key] = obj_id
            else:
                unique_key_value, start_end_tuple, models_dict = self.check_complex_key(key, json_schema)
                if unique_key_value:
                    converted_key, value_model_name = self.get_converted_complex_key(
                        unique_key_value, start_end_tuple, models_dict, obj_unique_key)
                    dct[converted_key] = dct.pop(key)
                    obj_id, _ = self.get_or_create_object(dct[converted_key], value_model_name)
                    dct[converted_key] = obj_id
        return dct

    def get_file_object(self, file_name: str) -> Any:
        """
        Получает путь к файлу (включая поддиректории) внутри передаваемой в класс папки со структурой данных.
        Возвращает объект File для записи в FileField
        """
        try:
            path = Path(self.tmp_folder + file_name)
            f = path.open(mode='rb')
            file_obj = File(f, name=path.name)
            self.opened_files.append(file_obj)
            return file_obj
        except Exception:
            pass
        return None

    def close_files(self) -> None:
        # Закрываем открытые файлы
        for f in self.opened_files:
            f.close()
        # Удаляем временную папку с фалами из архива
        if self.dir_path.exists() and self.dir_path.is_dir():
            shutil.rmtree(self.dir_path)

    def get_obj_key_value_dict(self, obj: Any, obj_unique_key: Any, model_name: str) -> dict:
        """ Возвращает словарь полей и их преобразованных значений переданного объекта для последующей записи в БД  """
        res = {}
        for k, v in obj.items():
            if k in self.models_dict[model_name]['fk_fields']:
                kk, vv = self.get_fk_key_and_value(k, v, obj_unique_key, model_name)
                res[kk] = vv
            elif k in self.models_dict[model_name]['obj_fields']:
                field_obj = self.models_dict[model_name]['obj_fields'][k]
                if type(field_obj) in (FileField, FieldFile):
                    res[k] = self.get_file_object(v)
                elif isinstance(field_obj, JSONField) and model_name in self.JSON_SCHEMAS and \
                        k in self.JSON_SCHEMAS[model_name]:
                    json_schema = self.JSON_SCHEMAS[model_name][k]
                    res[k] = self.convert_json_field_value(v, json_schema, obj_unique_key)
                else:
                    res[k] = v
        return res

    def update_object(self, obj: Any, db_obj: Any, key_value: str, model_name: str) -> bool:
        """ Обновляет объект в БД данными из структуры объектов """
        key_value_dict = self.get_obj_key_value_dict(obj, key_value, model_name)
        for k, v in key_value_dict.items():
            setattr(db_obj, k, v)
        try:
            db_obj.save()
        except Exception as err:
            self.log_message(f'!!! ОШИБКА ПРИ ОБНОВЛЕНИИ. Модель: {model_name}, объект {key_value}, {err}')
            return False
        return True

    def save_object(self, obj: Any, key_value: str, model_name: str):
        """ Преобразовывает поля объекта и записывает объект в БД """
        key_value_dict = self.get_obj_key_value_dict(obj, key_value, model_name)
        model = self.models_dict[model_name]['model']
        db_obj = model(
            **key_value_dict
        )
        try:
            db_obj.save()
        except Exception as err:
            self.log_message(f'!!! ОШИБКА ПРИ СОЗДАНИИ. Модель: {model_name}, объект {key_value}, {err}')
            return None
        return db_obj.id

    def get_obj_by_unique_key_value(self, obj_key_value: str, model_name: str) -> Any:
        if model_name in self.objects_dict and obj_key_value in self.objects_dict[model_name]:
            return self.objects_dict[model_name][obj_key_value]
        else:
            return None

    def get_or_create_object(self, obj_key_value: str, model_name: str, obj: Any = None) -> (Any, bool):
        """
        Возвращает ID объекта и признак того, что объект в данный момент уже обрабатывается (на одном из
        предыдущих уровней рекурсии)
        """
        if not obj_key_value:
            return None, False
        obj_id = None
        if model_name in self.created_objects and obj_key_value in self.created_objects[model_name]:
            return self.created_objects[model_name][obj_key_value], False

        if not obj:
            obj = self.get_obj_by_unique_key_value(obj_key_value, model_name)

        db_obj = self.get_object_from_db(obj_key_value, model_name)
        if obj:
            # если объект в обработке, то возвращаем ID из БД (если есть)
            if model_name in self.processed_objects and obj_key_value in self.processed_objects[model_name]:
                return (db_obj.id, True) if db_obj else (None, True)

            self.processed_objects[model_name][obj_key_value] = True
            if db_obj:
                self.update_object(obj, db_obj, obj_key_value, model_name)
                self.processed_objects[model_name].pop(obj_key_value, None)
                obj_id = db_obj.id
            else:
                obj_id = self.save_object(obj, obj_key_value, model_name)
                self.processed_objects[model_name].pop(obj_key_value, None)
            self.created_objects[model_name][obj_key_value] = obj_id
        elif db_obj:
            obj_id = db_obj.id
            self.created_objects[model_name][obj_key_value] = obj_id
        return obj_id, False

    def process_model_m2m_relations(self, model_name: str) -> None:  # NOSONAR
        """ Устанавливает many-to-many связи после обработки объектов """
        model = self.get_model_by_name(model_name)
        for key_value, obj in self.objects_dict[model_name].items():
            obj_id, _ = self.get_or_create_object(key_value, model_name, obj)
            if obj_id:
                db_obj = model.objects.filter(id=obj_id).first()
                for field_name, field in self.models_dict[model_name]['m2m_fields'].items():
                    if field_name in obj:
                        related_model_name = self.get_related_model_name(field)
                        m2m_obj_id_list = []
                        for m2m_obj_key_value in obj[field_name]:
                            m2m_obj_id, _ = self.get_or_create_object(m2m_obj_key_value, related_model_name)
                            if m2m_obj_id:
                                m2m_obj_id_list.append(m2m_obj_id)
                        m2m_relation = getattr(db_obj, field_name)
                        if self.CLEAR_OLD_M2M_RELATIONS:
                            m2m_relation.clear()
                        if m2m_obj_id_list:
                            related_model = self.get_model_by_name(related_model_name)
                            qs = related_model.objects.filter(id__in=m2m_obj_id_list)
                            m2m_relation.add(*qs)

    def process_model_objects(self, model_name: str) -> None:
        """  Обрабатывает все объекты модели"""
        for key_value, obj in self.objects_dict[model_name].items():
            self.log_message(f'Обрабатывается {model_name}, объект {key_value}')
            obj_id, _ = self.get_or_create_object(key_value, model_name, obj)

    def model_has_m2m_fields(self, model_name: str) -> bool:
        if self.models_dict[model_name]['m2m_fields']:
            return True
        return False

    def process_m2m_relations(self) -> None:
        self.log_message(f'Обрабатываются m2m-связи...')
        for model_name in self.objects_dict:
            if self.model_has_m2m_fields(model_name):
                self.process_model_m2m_relations(model_name)

    def process_objects(self) -> (bool, str):
        msg = 'Структура связанных объектов успешно сохранена в БД'
        status = DocTypeImportLog.SUCCESS
        success = True
        try:
            for model_name in self.objects_dict:
                self.process_model_objects(model_name)
            self.process_m2m_relations()
        except Exception as err:
            msg = f'Произошла ошибка при загрузке {err}'
            status = DocTypeImportLog.ERROR
            success = False
        self.close_files()
        self.log_message(msg)
        self.save_log_record(status=status)
        return success, msg
