class EavViewSet(QueryArgumentsMixin, BaseMixin, viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    swagger_schema = None
    filter_backends = [filters.DjangoFilterBackend, EavModelOrdering, EavModelSearch]
    filterset_class = EavModelFilter

    ...
    ...

    def _send_to_bus(self, data, action_name):
        # для последующего преобразования в JSON
        for key in list(data.keys()):
            if isinstance(data[key], Decimal):
                data[key] = float(data[key])

        model_class, doc_type_obj = self.get_model_and_types()
        if doc_type_obj and doc_type_obj.pass_data_to_bus:
            try:
                DoctypeBusExchange.send(
                    doc_type_obj.code,
                    data,
                    action_name,
                )
                # отправка в ДИТ
                model_name = self.kwargs.get('model', '')
                if settings.SEND_TO_DIT_BUS and model_name in DITObjectDTO.MODELS_DICT:
                    action_send = DITObjectDTO.ACTION_CODE_CREATE if action_name == 'create' \
                        else DITObjectDTO.ACTION_CODE_UPDATE

                    obj = DITObjectDTO()
                    obj.entityCode = DITObjectDTO.get_entity_code(model_name)
                    obj.systemObjectId = str(data['id'])
                    obj.actionCode = action_send
                    obj.address = {}
                    obj.coordinates = None
                    obj.geometryType = None
                    obj.extendedData = data
                    DITBus.send(obj)
            except Exception:
                pass

    def _send_to_bus_from_serializer(self, serializer, action_name):
        data = serializer.data
        self._send_to_bus(data, action_name)

    def perform_create(self, serializer):
        raise_msg = self._save_serializer_with_permissions(serializer)
        if not raise_msg:
            # отправка данных на шину
            self._send_to_bus_from_serializer(serializer, 'create')
        return raise_msg

    ...
    ...
