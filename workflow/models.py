from django.db import models


class DocType(ConfiguratorMixin):
    """ Тип представления """

    EAV_VALUES_BASE_TABLE = 'eav_value'

    FIELD_DOC_TYPE = 'doc_type'

    CACHE_MODEL_PREFIX = 'doc_type_eav_value'

    LANDING_DOC_TYPE_CODE = 'landings'

    DJANGO_MODEL_TT = 0
    VIEW_TT = 1
    CLICKHOUSE_TT = 2

    TABLE_TYPE_CHOICES = (
        (DJANGO_MODEL_TT, 'Модель django'),
        (VIEW_TT, 'Представление'),
        (CLICKHOUSE_TT, 'Модель clickhouse'),
    )
    datetime_create = models.DateTimeField(u'Время добавления', auto_now_add=True, null=True, )
    datetime_update = models.DateTimeField(u'Время обновления', auto_now=True, null=True, )
    name = models.CharField('Наименование', max_length=255)
    title = models.CharField('Заголовок', null=True, blank=True, max_length=255)
    name_plural = models.CharField('Наименование (мн. число)', null=True, blank=True, max_length=255)
    code = models.CharField('Код', max_length=250, unique=True)
    is_active = models.BooleanField(default=True, verbose_name='Активен')
    in_history = models.BooleanField(default=False, verbose_name='Хранить историю')
    show_doc_templates = models.BooleanField(default=False, verbose_name='Показывать печатные формы')
    has_chat = models.BooleanField(default=False, verbose_name='Разрешить обсуждение')
    pass_data_to_bus = models.BooleanField(default=False, verbose_name='Передавать данные на шину')
    receive_data_from_bus = models.BooleanField(default=False, verbose_name='Получать данные с шины')
    disable_delete = models.BooleanField(default=False, verbose_name='Запретить удаление')
    disable_update = models.BooleanField(default=False, verbose_name='Запретить изменение')
    attributes = models.ManyToManyField("eav.Attribute", blank=True, verbose_name='Доступные атрибуты',
                                        related_name='doctypes')
    statuses = models.ManyToManyField("workflow.DocStatus", blank=True,
                                      verbose_name='Доступные статусы', related_name='doc_types')
    default_status = models.ForeignKey("workflow.DocStatus", blank=True,
                                       related_name='as_default_for',
                                       verbose_name='Статус по-умолчанию', null=True, on_delete=models.SET_NULL)
    table = models.ForeignKey(ContentType, on_delete=models.CASCADE, verbose_name='Таблица',
                              help_text='По умолчанию - workflow.DocItem', null=True, blank=True)
    table_ch = models.CharField(null=True, blank=True, verbose_name='Таблица clickhouse', max_length=250)
    table_type = models.IntegerField(choices=TABLE_TYPE_CHOICES, default=0, verbose_name='Тип таблицы')
    table_raw_sql = models.TextField(null=True, blank=True, verbose_name='Sql запрос', )
    values_in_separate_table = models.BooleanField(default=False, verbose_name='Хранить значения в отдельной таблице',
                                                   help_text='Для изменения значений воспользуйтесь командой в списке')
    doc_templates = models.ManyToManyField(DocTemplate, blank=True, verbose_name='Печатные формы',
                                           through='DocTemplateDocTypeMembership')

    class Meta:
        verbose_name = 'Тип представления'
        verbose_name_plural = '1. Типы представлений'
        ordering = ('name',)


class DocTypeImportLog(models.Model):
    OPERATION_EXPORT = 1
    OPERATION_IMPORT = 2

    IN_PROGRESS = 1
    SUCCESS = 2
    ERROR = 3

    OPERATIONS = [
        (OPERATION_EXPORT, 'Экспорт Типов представлений из БД'),
        (OPERATION_IMPORT, 'Импорт Типов представлений из zip-архива'),
    ]

    STATUSES = [
        (IN_PROGRESS, 'В процессе'),
        (SUCCESS, 'Успешно завершен'),
        (ERROR, 'Завершен с ошибкой'),
    ]

    operation = models.IntegerField(verbose_name='Операция', choices=OPERATIONS, default=OPERATION_IMPORT)
    start = models.DateTimeField(verbose_name='Начало выполнения')
    end = models.DateTimeField(verbose_name='Окончание выполнения', null=True, blank=True)
    duration = models.IntegerField(verbose_name='Продолжительность', null=True, blank=True)
    status = models.IntegerField(verbose_name='Статус', choices=STATUSES, default=IN_PROGRESS)
    execution_log = models.TextField(verbose_name='Лог выполнения', null=True, blank=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Автор')
    zip_file = models.FileField(upload_to='data_structures', null=True, blank=True,
                                verbose_name='ZIP-архив со структурой данных')
    file = models.ForeignKey(UploadedFile, verbose_name='Выгруженный файл', null=True, blank=True,
                             on_delete=models.SET_NULL)
    high_level_objects = ArrayField(models.CharField(max_length=50), null=True, blank=True,
                                    verbose_name='Выгружаемые объекты верхнего уровня')

    class Meta:
        verbose_name = 'Лог переноса структуры данных'
        verbose_name_plural = '1.2. Логи переноса структуры данных'

    def __str__(self):
        operation_name = list(filter(lambda x: x[0] == self.operation, self.OPERATIONS))[0][1]
        return f'{operation_name} {self.start:%Y-%m-%d %H:%M}'


def get_default_unique_key_fields():
    return ['code', 'name', 'slug']


def get_default_app_labels():
    return ['workflow', 'form_builder', 'rules', 'contenttypes', 'eav', 'showcase_builder']


def get_default_excluded_models():
    return [
        'workflow.LogEvent',
        'workflow.DocItem',
        'workflow.DocTemplateScanSignatory',
        'workflow.DocTemplateScan',
        'workflow.DocTemplateDocTypeMembership',
        'workflow.DocTypeImportLog',
        'eav.Value',
        'showcase_builder.MatViewTableLog',
    ]


def get_default_json_schemas():
    form_item = 'form_builder.formitem'
    return {
        form_item: {
            'config': {
                'form_list': form_item,
                'form_object': form_item,
                'form_edit': form_item,
                'form_create': form_item,
                'form_select_from_list': form_item,
                'map_list': form_item,
                'type_print_form': 'workflow.doctemplate',
                'connected_data': form_item,
                'type_view': 'workflow.doctype',
                'available_content_types': 'workflow.doctype',
                'td_<workflow.doctype>_form_list': form_item,  # id в названии ключа в треугольны скобках
            },
        },
    }


class DataStructureSettings(models.Model):
    name = models.CharField(verbose_name='Название', max_length=255)
    date_time = models.DateTimeField(verbose_name='Дата-время создания', auto_now=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Автор')
    active = models.BooleanField(verbose_name='Активен', default=True)
    # Общие
    separator_char = models.CharField(verbose_name='Разделитель значений комплексного ключа', max_length=1, default='&')
    # Экспорт
    default_unique_key_fields = ArrayField(models.CharField(max_length=20), verbose_name='Уникальные поля по умолчанию',
                                           default=get_default_unique_key_fields)
    default_app_labels = ArrayField(models.CharField(max_length=20), default=get_default_app_labels,
                                    verbose_name='Приложения Django для выгрузки структуры')
    excluded_models = ArrayField(models.CharField(max_length=50), default=get_default_excluded_models,
                                 verbose_name='Модели-исключения')
    exclude_high_level_m2m = models.BooleanField(verbose_name='Исключить m2m-связи с моделями верхнего уровня',
                                                 default=True)
    only_start_objects = models.BooleanField(verbose_name='Выгружать только переданные объекты верхнего уровня',
                                             default=True)
    tree_traversal_straight_down = models.BooleanField(verbose_name='Обход дерева связей строго вниз',
                                                       default=True)
    json_schemas = models.JSONField(verbose_name='JSON-схема для подстановки значений вместо ID',
                                    default=get_default_json_schemas)
    # Импорт
    ignore_json_errors = models.BooleanField(verbose_name='Игнорировать ошибки преобразования JSON-полей',
                                             default=False)
    clear_old_m2m_relations = models.BooleanField(verbose_name='Удалять старые m2m-связи',
                                                  default=False)

    class Meta:
        verbose_name = 'Настройки переноса структуры данных'
        verbose_name_plural = '1.3. Настройки переноса структуры данных'

    def __str__(self):
        is_active = 'Активен' if self.active else 'Не активен'
        return f'{self.name} - {is_active}'
