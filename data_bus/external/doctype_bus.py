import logging

from data_bus.models import BusDto
from data_bus.services import BusRabbitService
from workflow.models import DocType

log = logging.getLogger('b3.doctype_bus')


class DoctypeBusExchange:

    PREFIX = 'platform__'

    @classmethod
    def send(cls, doc_type: str, data: dict, event_type: str):
        service = BusRabbitService()
        event = BusDto()
        event.type = event_type
        event.data = data
        event.source = f'{cls.PREFIX}{doc_type}'
        service.send(event)

        log.info(f'Sent message from {event.source}: {data}')

    @classmethod
    def callback(cls, event: BusDto):

        if event.source.startswith(cls.PREFIX):
            doc_type_code = event.source.split(cls.PREFIX)[1]
            doc_type = DocType.objects.filter(code=doc_type_code).first()
            if doc_type and doc_type.receive_data_from_bus:
                doc_type.update_model_from_bus(action=event.type, data=event.data)

            log.info(f'Received message from {event.source}')
