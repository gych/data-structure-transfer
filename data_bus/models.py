import json

from .exceptions import BusException


class BasicDataDto:
    def to_dict(self):
        raise BusException


class BusDto:
    TARGET_DIT = 'dit'

    TYPE_EVENT = 'event'
    TYPE_SIGNAL = 'signal'

    type = TYPE_EVENT
    source = None
    target = None
    data: dict = {}

    @staticmethod
    def get_from_json(data):
        d = BusDto()
        d.type = data.get('type', BusDto.TYPE_EVENT)
        if data.get('data'):
            d.source = data.get('source')
            d.target = data.get('target')
            d.data = data.get('data')
        else:
            d.data = data

        return d

    def to_json(self):
        return json.dumps({
            'type': self.type,
            'source': self.source,
            'target': self.target,
            'data': self.data,
        })

