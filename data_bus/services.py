import json
import logging
import uuid
from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING

import pika
import pika.exceptions
from django import db
from django.conf import settings
from pika.exceptions import AMQPError

from .external.dit import DITObjectDTO
from .models import BusDto

log = logging.getLogger('b3.data_bus')

if TYPE_CHECKING:
    from typing import Callable, List


class BusBasicService(metaclass=ABCMeta):
    callbacks: 'List[Callable]' = []

    NODE_NAME: str = settings.SERVER_NAME
    BASIC_TOPIC: str = 'bus'
    BASIC_QUEUE: str = 'bus_events'

    def __init__(self, queue_name: str = 'bus_events'):
        self.BASIC_QUEUE = queue_name

    @abstractmethod
    def send(self, event_data: BusDto):
        pass

    @abstractmethod
    def listen(self):
        pass

    @property
    def queue_name(self):
        return f'{self.NODE_NAME}_{self.BASIC_QUEUE}'


class BusRabbitService(BusBasicService):
    connection = None
    channel = None
    response = None
    corr_id = None
    callback = None

    PERSISTENT_MESSAGE = 2

    EXCHANGE_DELETED_OK = 'Exchange.DeleteOk'

    def _reconnect(self):
        self.connection = pika.BlockingConnection(pika.URLParameters(settings.RABBIT_MQ_CONN_BUS))
        self.channel = self.connection.channel()

    def __init__(self, queue_name):
        super().__init__(queue_name=queue_name)
        self._reconnect()

    def _exchange_declare(self, exchange: str = '', exchange_type: str = ''):
        exchange = exchange or self.BASIC_TOPIC
        exchange_type = exchange_type or 'topic'
        self.channel.exchange_declare(exchange, durable=True, exchange_type=exchange_type)
        return exchange

    def exchange_delete(self, exchange_name):
        result = self.channel.exchange_delete(exchange=exchange_name)
        if result.method.NAME == self.EXCHANGE_DELETED_OK:
            return True
        else:
            return False

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body
            self.callback(body)

    def send(
            self,
            event_data: BusDto,
            exchange: str = '',
            exchange_type: str = '',
            routing_key: str = '*',
            reply_callback=None
    ):
        self.corr_id = str(uuid.uuid4())

        try:
            exchange = self._exchange_declare(exchange, exchange_type)
            reply_to = None
            self.callback = reply_callback
            self.response = None

            if self.callback:
                reply_queue = self.channel.queue_declare(queue='', exclusive=True)
                reply_to = reply_queue.method.queue

                self.channel.basic_consume(
                    queue=reply_to,
                    on_message_callback=self.on_response,
                    auto_ack=True)

            self.channel.basic_publish(
                exchange=exchange,
                body=event_data.to_json(),
                routing_key=routing_key,
                properties=pika.BasicProperties(
                    correlation_id=self.corr_id,
                    delivery_mode=self.PERSISTENT_MESSAGE,
                    reply_to=reply_to
                )
            )

            if self.callback:
                while self.response is None:
                    self.connection.process_data_events()

        except pika.exceptions.StreamLostError:
            self._reconnect()

        return True

    def listen(self, exchange: str = '', exchange_type: str = '', routing_key: str = '*'):
        exchange = self._exchange_declare(exchange, exchange_type)
        self.channel.queue_declare(self.queue_name, durable=True)
        self.channel.queue_bind(self.queue_name, exchange, routing_key=routing_key)

        while True:
            self.channel.basic_consume(
                queue=self.queue_name,
                on_message_callback=self._callback_rabbit,
            )

            log.info('[*] Waiting for messages')
            try:
                self.channel.start_consuming()
            except AMQPError:
                log.warning('Connection lost')

    def _callback_rabbit(self, channel, method, properties, body):
        body = json.loads(body)
        for conn in db.connections.all():
            conn.close_if_unusable_or_obsolete()

        at_least_one_accepted = False
        for callback in self.callbacks:
            try:
                callback(BusDto.get_from_json(body))
                at_least_one_accepted = True
            except Exception as e:
                log.error('Error "%s" while handling callback "%s"', e, callback)
                log.exception(e)

        if at_least_one_accepted and channel.is_open:
            channel.basic_ack(method.delivery_tag)

    def register_callback(self, callback):
        return self.callbacks.append(callback)

    def send_to_dit(
            self,
            action,
            entity_code: str,
            entity_id: str,
            address: dict,
            coordinates: list,
            extended_data: dict,
            event_status: str,
            instance_datetime: str = '',
            instance_datetime_from: str = '',
            instance_datetime_to: str = '',
            value: float = 0
    ):
        d = BusDto()
        d.source = self.NODE_NAME
        d.target = BusDto.TARGET_DIT

        o = DITObjectDTO()
        o.entityCode = entity_code
        o.systemObjectId = entity_id
        o.actionCode = action
        o.address = address
        o.value = value
        o.isInstanceTimePresent = int(bool(instance_datetime))
        o.isPeriodStartTimePresent = int(bool(instance_datetime_from))
        o.isPeriodEndTimePresent = int(bool(instance_datetime_to))
        o.instanceDateTime = instance_datetime
        o.periodStartDateTime = instance_datetime_from
        o.periodEndDateTime = instance_datetime_to

        o.eventStatus = event_status
        o.coordinates = coordinates
        o.geometryType = 'Point'
        o.extendedData = extended_data
        d.data = o.to_dict()

        try:
            self.send(d)
        except Exception as err:
            log.error('Error "%s" sending to DIT=%s', err, d.to_json())
            log.exception(err)


def get_bus_service(queue_name: str = 'bus_events'):
    return BusRabbitService(queue_name)
