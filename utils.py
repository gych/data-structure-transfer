import decimal
import tempfile

import googlemaps as googlemaps
from googlemaps.maps import StaticMapMarker

from PIL import Image


def get_map_image_with_marker(lat: decimal,
                              lon: decimal,
                              zoom: int = MAP_IMAGE_ZOOM,
                              size: int = MAP_IMAGE_SIZE,
                              logo_size: int = MAP_IMAGE_LOGO_SIZE) -> tempfile.NamedTemporaryFile:
    client = googlemaps.Client(key=GOOGLE_MAP_API_KEY)

    image_file = tempfile.NamedTemporaryFile(suffix='.png')
    marker_obj = [lat, lon]
    markers = StaticMapMarker([marker_obj])
    chunks = client.static_map(size=(size, size),
                               center=(lat, lon),
                               zoom=zoom,
                               markers=markers,
                               language='RU')
    for chunk in chunks:
        if chunk:
            image_file.write(chunk)

    im = Image.open(image_file)
    xy_lower = size - logo_size
    im_crop = im.crop((0, 0, xy_lower, xy_lower))
    im_crop.save(image_file.name)
    return image_file
